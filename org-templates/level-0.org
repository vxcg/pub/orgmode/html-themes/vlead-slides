#+INCLUDE: "./preamble.org"
# #+INCLUDE: "./pre.org"

#+BEGIN_EXPORT html

<div class="footer-right"><img src="./themes/vlead-slides/style/img/iiit-logo.png" height="130"></img></div>
<div class="header-right"><img src="./themes/vlead-slides/style/img/vlabs-color-large.png" height="130"></img></div>
<div class="header-left"><img src="./themes/vlead-slides/style/img/vlead-color-large.png" height="130"></img></div>
<!--    <div class="footer-left"></div> -->
	 <script type="text/javascript" src="./themes/vlead-slides/style/js/numformat.js"></script>
	 <script type="text/javascript" src="./themes/vlead-slides/style/js/event-hooks.js"></script> 
	 <script type="text/javascript" src="./themes/vlead-slides/style/js/lib/jquery-2.2.3.min.js"></script> 
	 <script type="text/javascript" src="./themes/vlead-slides/style/js/doc-ready-fn.js"></script> 
<!-- 	 <div class="watermark">DRAFT of <span id="date"></span></div>  -->

#+END_EXPORT

