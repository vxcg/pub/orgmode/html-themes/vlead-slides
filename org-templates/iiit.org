#+INCLUDE: "./preamble.org"
# #+INCLUDE: "./pre.org"

#+BEGIN_EXPORT html
<script type="text/x-mathjax-config">
      MathJax.Hub.Config({
        TeX: { extensions: ["color.js","autoload-all.js"] }
      });
    </script>

<div class="header-right"> <a href="https://iiit.ac.in"><img border="0" src="./themes/vlead-slides/style/img/iiit-logo.png" height="130"></a></div>
<!--    <div class="footer-left"></div> -->
	 <script type="text/javascript" src="./themes/vlead-slides/style/js/numformat.js"></script>
	 <script type="text/javascript" src="./themes/vlead-slides/style/js/event-hooks.js"></script> 
	 <script type="text/javascript" src="./themes/vlead-slides/style/js/lib/jquery-2.2.3.min.js"></script> 
	 <script type="text/javascript" src="./themes/vlead-slides/style/js/doc-ready-fn.js"></script> 
<!-- 	 <div class="watermark">DRAFT of <span id="date"></span></div>  -->

#+END_EXPORT

