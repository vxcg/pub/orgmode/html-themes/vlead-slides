#+MACRO: NEWLINE @@latex:\\@@ @@html:<br/>@@
#+MACRO: SPACE @@latex:\ @@ @@html:&nbsp;@@
#+MACRO: INR @@latex:\rupee @@ @@html:&#8377;@@
#+TAGS: pics(p) experts(e)
# #+EXCLUDE_TAGS: pics
#+BIND: org-html-doctype "html5"
# #+REVEAL_ROOT: ./themes/org-reveal/reveal.js
#+OPTIONS: toc:t title:t
#+OPTIONS: reveal_center:t reveal_progress:t reveal_history:nil reveal_control:nil
#+OPTIONS: reveal_rolling_links:nil reveal_keyboard:t reveal_overview:t num:nil
#+OPTIONS: reveal_width:1200 reveal_height:800
#+OPTIONS: reveal_slide_number:c/t
#+OPTIONS: reveal_single_file:nil


#+REVEAL_MARGIN: 0.1

#+REVEAL_TRANS: fade*
#+REVEAL_THEME: white
#+REVEAL_HLEVEL: 1
#+REVEAL_HEAD_PREAMBLE: <meta name="description" content="Presentation">
#+REVEAL_POSTAMBLE: <div class="postamble">%T</div>
#+REVEAL_PLUGINS: (markdown notes toc-progress)
#+REVEAL_EXTRA_CSS:  ./themes/vlead-slides/style/css/local.css

# plugins MUST BE in the directory under src for things to
# work.  Not sure why.

# #+REVEAL_EXTRA_JS:  {src: './plugin/toc-progress/toc-progress.js', async: true, callback: function() { toc_progress.initialize(); toc_progress.create(); } }
# #+REVEAL_EXTRA_JS:  {src: './themes/org-reveal/Reveal.js-TOC-Progress/plugin/toc-progress/toc-progress.js', async: true, callback: function() { toc_progress.initialize(); toc_progress.create(); } }
#  This technique of setting headers and footers is from 
# http://stackoverflow.com/questions/34706859/set-header-and-footer-reveal-js-presentation

